package org.example;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import com.fasterxml.jackson.core.JsonProcessingException;
import java.util.HashMap;
import java.util.Map;
import org.junit.jupiter.api.Test;

class PipelineDescriptorTest {


    @Test
    void processDocumentByDescriptor_GetModifiedDocument() throws JsonProcessingException {
        Map<String, Object> initialDocument = new HashMap<>();
        Map<String, Object> expectedDocument = Map.of("firstName", "George", "name", "Brante", "numOfFields", 4, "age", 42, "email", "brante@ukr.net");
        initialDocument.put("name", "Brante");
        initialDocument.put("age", 42);
        initialDocument.put("email", "brante@ukr.net");

        String descriptorJSON = """
            {
                "steps": [
                    {
                        "processor": "AddField",
                        "configuration": {
                            "fieldName": "firstName",
                            "fieldValue": "George"
                        }
                    },
                    {
                        "processor": "CountNumOfFields",
                        "configuration": {
                            "targetFieldName": "numOfFields"
                        }
                    }
                ]
            }""";
        PipelineDescriptor descriptor = PipelineDescriptor.fromJson(descriptorJSON);
        ProcessFactory processFactory = new DefaultProcessorFactory();
        PipelineExecutor pipelineExecutor = new PipelineExecutor(processFactory);

        pipelineExecutor.transform(descriptor, initialDocument);
        System.out.println(initialDocument);
        assertEquals(initialDocument.size(), expectedDocument.size());
        assertEquals(initialDocument.entrySet(), expectedDocument.entrySet());
    }

}