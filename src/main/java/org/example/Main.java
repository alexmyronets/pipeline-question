package org.example;

import com.fasterxml.jackson.core.JsonProcessingException;
import org.example.PipelineDescriptor.Step;

public class Main {

    public static void main(String[] args) throws JsonProcessingException {
        String json = """
            {
            "steps": [
            {
            "processor": "AddField",
            "configuration" : {
            "fieldName": "accountName",
            "fieldValue": "Facebook"
            }
            },
            {
            "processor": "RemoveField",
            "configuration" : {
            "fieldName": "userName"
            }
            },
            {
            "processor": "AddField",
            "configuration" : {
            "fieldName": "userName",
            "fieldValue": "David"
            }
            },
            {
            "processor": "CountNumOfFields",
            "configuration" : {
            "targetFieldName": "numOfFields"
            }
            }
            ]
            }""";
        var descriptor = PipelineDescriptor.fromJson(json);

        for (Step step: descriptor.getSteps()) {
            System.out.println(step.getProcessor());
            System.out.println(step.getConfiguration());
        }
    }
}