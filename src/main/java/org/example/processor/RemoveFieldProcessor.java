package org.example.processor;

import java.util.Map;
import org.example.processor.Processor;

public class RemoveFieldProcessor implements Processor {
    private String fieldToRemove;

    @Override
    public void initialize(Map<String, String> configuration) {
        this.fieldToRemove = configuration.get("fieldName");
    }

    @Override
    public void process(Map<String, Object> jsonDocument) {
            jsonDocument.remove("fieldToRemove");
    }
}
