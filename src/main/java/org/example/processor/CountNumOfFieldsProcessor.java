package org.example.processor;

import java.util.Map;
import org.example.processor.Processor;

public class CountNumOfFieldsProcessor implements Processor {
    private String fieldName;

    @Override
    public void initialize(Map<String, String> configuration) {
        this.fieldName = configuration.get("targetFieldName");
    }

    @Override
    public void process(Map<String, Object> jsonDocument) {
        int count = jsonDocument.size();
        jsonDocument.put(fieldName, count);
    }
}
