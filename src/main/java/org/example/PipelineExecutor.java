package org.example;

import java.util.Map;
import org.example.PipelineDescriptor.Step;
import org.example.processor.Processor;

public class PipelineExecutor {
    private final ProcessFactory processFactory;

    public PipelineExecutor(ProcessFactory processFactory) {
        this.processFactory = processFactory;
    }

    public void transform(PipelineDescriptor pipelineDescriptor, Map<String, Object> jsonDocument) {
        for (Step step: pipelineDescriptor.getSteps()) {
            Processor processor = processFactory.create(step.getProcessor());
            processor.initialize(step.getConfiguration());
            processor.process(jsonDocument);
        }
    }

}
