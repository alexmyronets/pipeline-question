package org.example;

import org.example.processor.Processor;

public interface ProcessFactory {

    Processor create(String processorName);
}
