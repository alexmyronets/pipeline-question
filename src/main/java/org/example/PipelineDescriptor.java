package org.example;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class PipelineDescriptor {
    private List<Step> steps = new ArrayList<>();

    public static PipelineDescriptor fromJson(String json) {
        ObjectMapper objectMapper = new ObjectMapper();
        try {
            return objectMapper.readValue(json, PipelineDescriptor.class);
        } catch (JsonProcessingException e) {
            throw new RuntimeException(e);
        }
    }

    @Setter
    @Getter
    public static class Step {
        private String processor;
        private Map<String, String> configuration;
    }
}
