package org.example;

import org.example.processor.AddFieldProcessor;
import org.example.processor.CountNumOfFieldsProcessor;
import org.example.processor.Processor;
import org.example.processor.RemoveFieldProcessor;

public class DefaultProcessorFactory implements ProcessFactory{

    @Override
    public Processor create(String processorName) {
        switch (processorName) {
            case "AddField" -> {
                return new AddFieldProcessor();
            }
            case "RemoveField" -> {
                return new RemoveFieldProcessor();
            }
            case "CountNumOfFields" -> {
                return new CountNumOfFieldsProcessor();
            }
            default -> {
                return null;
            }
        }
    }
}
